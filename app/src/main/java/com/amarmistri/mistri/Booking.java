package com.amarmistri.mistri;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loopj.android.http.RequestParams;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Booking extends AppCompatActivity {

    protected Button submit_button;
    String myString = "Sucessfully Connected";
    String myString2 = "Sorry! Not Connected";
    private  String name,phone,address,category,service,road,block,home;

    private EditText mName,mPhone,mHome;
    Spinner spinner_service , spinner_category, spinner_address,spinner_road,spinner_block;


    JsonRequest requestBody;

    RequestQueue queue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);


        queue = Volley.newRequestQueue(this);

        submit_button = (Button) findViewById(R.id.submit);

        mName = (EditText) findViewById(R.id.etname);
        mPhone = (EditText) findViewById(R.id.etphone);
        mHome = (EditText) findViewById(R.id.ethome);


        spinner_service = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.service_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_service.setAdapter(adapter);


        spinner_category = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.category_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_category.setAdapter(adapter2);

        spinner_address = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                R.array.address_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_address.setAdapter(adapter3);

        spinner_road = (Spinner) findViewById(R.id.spinner4);
        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this,
                R.array.road_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_road.setAdapter(adapter4);

        spinner_block = (Spinner) findViewById(R.id.spinner_block);
        ArrayAdapter<CharSequence> adapter5 = ArrayAdapter.createFromResource(this,
                R.array.block_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_block.setAdapter(adapter5);


        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = mName.getText().toString();
                phone = mPhone.getText().toString();
                home = mHome.getText().toString();


                address = spinner_address.getSelectedItem().toString();
                service = spinner_service.getSelectedItem().toString();
                category = spinner_category.getSelectedItem().toString();
                road = spinner_road.getSelectedItem().toString();
                block = spinner_block.getSelectedItem().toString();
                Log.d("Pritom", "Value: " + name + " " + phone + " " + address + " " + service + " " + category + " " + road + " " + block + " " + home);

                UserModel umodel = new UserModel();

                umodel.name = name;
                umodel.phone = phone;
                umodel.address = address;
                umodel.service = service;
                umodel.category = category;
                umodel.road = road;
                umodel.block = block;
                umodel.home = home;

                DataInsert(umodel);
            }
        });



    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Booking.this);
        builder.setMessage("Are you want to exit Booking?");
        builder.setCancelable(true);
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                finish();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    public RequestParams DataInsert(final UserModel model){

        String url = "http://amarmistriapi.ennvisiodigital.tech/amrmistri.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("UResponse", response);
                        Toast.makeText(getApplicationContext(), myString, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText( getApplicationContext(),myString2,Toast.LENGTH_LONG).show();
                    }
                }
        )


        {
            @Override
            protected Map<String,String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put( "name",model.name );
                params.put( "phone",model.phone );
                params.put( "address",model.address );
                params.put( "service",model.service );
                params.put( "category",model.category );
                params.put( "road",model.road );
                params.put( "block",model.block );
                params.put( "home",model.home );

                return params;
            }
        };



        queue.add(postRequest);

        return null;

    }
}
